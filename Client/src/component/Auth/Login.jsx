import React, { Component } from "react";
import Navbar from "../Home/Navbar";
import { Link } from "react-router-dom";

export class Login extends Component {
  state = {
    id: "",
    password: "",
    position: "",
  };
  submitHandler = e => {
    e.preventDefault();
  };
  changeHandler = e => {
    this.setState({ [e.target.name]: e.target.value });
  };
  render() {
    return (
      <div>
        <Navbar />
        <div className="container">
          <div className="row">
            <div className="col-md-3"></div>
            <div className="col-md-6 mt-5">
              <div id="about" className="about">
                <div className="card">
                  <div className="card-header text-center">Login</div>
                  <div className="list-group list-group-flush">
                    <div className="list-group-item">
                      <form onSubmit={this.submitHandler}>
                        <div className="form-group">
                          <label htmlFor="exampleInputEmail1">Library Id</label>
                          <input
                            type="text"
                            className="form-control"
                            name="id"
                            value={this.state.id}
                            onChange={this.changeHandler}
                            aria-describedby="emailHelp"
                            placeholder="Enter Library Id"
                          />
                          <small
                            id="emailHelp"
                            className="form-text text-muted"
                          >
                            We'll never share your email with anyone else.
                          </small>
                        </div>

                        <div className="form-group">
                          <label htmlFor="exampleInputPassword1">
                            Position
                          </label>
                          <select class="form-control" onChange={this.changeHandler} name="position">
                            <option value="">Default select</option>
                            <option value="student">Student</option>
                            <option value="librarian">Librarian</option>
                            <option value="admin">Admin</option>
                          </select>
                          <small
                            id="position Help"
                            className="form-text text-muted"
                          >
                            your Position
                          </small>
                        </div>
                        <div className="form-group">
                          <label htmlFor="exampleInputPassword1">
                            Password
                          </label>
                          <input
                            type="password"
                            className="form-control"
                            name="password"
                            value={this.state.password}
                            placeholder="Password"
                            onChange={this.changeHandler}
                          />
                        </div>

                        <button
                          type="submit"
                          className="btn btn-block btn-primary"
                        >
                          Login
                        </button>
                      </form>
                      <small
                        id="registarHelp"
                        className="mt-2 text-right form-text text-muted"
                      >
                        Don't Have Account? <Link to="/registar">Registar</Link>{" "}
                        here!.
                      </small>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Login;
