import React, { Component } from "react";
import Navbar from "../Home/Navbar";
import { Link } from "react-router-dom";

export class Registar extends Component {
  state = {
    email: "",
    id: "",
    password: "",
    confirm_password: ""
  };
  changeHandler = e => {
    this.setState({ [e.target.name]: e.target.value });
  };
  submitHandler = e => {
    e.preventDefault();
  };
  render() {
    return (
      <div>
        <Navbar />
        <div className="container">
          <div className="row">
            <div className="col-md-3"></div>
            <div className="col-md-6 mt-5">
              <div id="about" className="about">
                <div className="card">
                  <div className="card-header text-center">Registration</div>
                  <div className="list-group list-group-flush">
                    <div className="list-group-item">
                      <form onSubmit={this.submitHandler}>
                        <div className="form-group">
                          <label htmlFor="exampleInputEmail1">
                            Email Address
                          </label>
                          <input
                            type="email"
                            className="form-control"
                            name="email"
                            value={this.state.email}
                            onChange={this.changeHandler}
                            aria-describedby="emailHelp"
                            placeholder="Enter Email Address"
                          />
                          <small
                            id="emailHelp"
                            className="form-text text-muted"
                          >
                            We'll never share your email with anyone else.
                          </small>
                        </div>
                        <div className="form-group">
                          <label htmlFor="exampleInputEmail1">Library Id</label>
                          <input
                            type="text"
                            className="form-control"
                            name="id"
                            value={this.state.id}
                            onChange={this.changeHandler}
                            aria-describedby="emailHelp"
                            placeholder="Enter Library Id"
                          />
                          <small
                            id="emailHelp"
                            className="form-text text-muted"
                          >
                            We'll never share your email with anyone else.
                          </small>
                        </div>
                        <div className="form-group">
                          <label htmlFor="exampleInputPassword1">
                            Password
                          </label>
                          <input
                            type="password"
                            className="form-control"
                            name="password"
                            value={this.state.password}
                            placeholder="Password"
                            onChange={this.changeHandler}
                          />
                        </div>
                        <div className="form-group">
                          <label htmlFor="exampleInputPassword1">
                            Password
                          </label>
                          <input
                            type="password"
                            className="form-control"
                            name="password"
                            value={this.state.confirm_password}
                            placeholder=" Confirm Password"
                            onChange={this.changeHandler}
                          />
                        </div>

                        <button
                          type="submit"
                          className="btn btn-block btn-primary"
                        >
                          Registar
                        </button>
                      </form>
                      <small
                        id="registarHelp"
                        className="mt-2 text-right form-text text-muted"
                      >
                        Already Have Account? <Link to="/login">Login</Link>{" "}
                        here!.
                      </small>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Registar;
