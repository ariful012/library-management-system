import React, { Component } from "react";

export class Footer extends Component {
  render() {
    return (
      <div className="footer">
        <footer>
          <p>Posted by: Lorem Ipsum</p>
          <p>
            <a target="_blank" href="http://www.hamdarduniversity.edu.bd/">
              &copy; Hamdard University Library
            </a>
            .
          </p>
        </footer>
      </div>
    );
  }
}

export default Footer;
