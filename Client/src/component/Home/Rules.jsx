import React, { Component } from "react";

export class Rules extends Component {
  render() {
    return (
      <div id="rules" className="card m-3 text-center">
        <div className="card-header">Rules & Regulations</div>
        <ul className="list-group list-group-flush">
          <li className="list-group-item">
            Identity Card is compulsory for getting access to the
            library.Silence to be maintained.No discussion permitted inside the
            library.Registration should be done to become a library member prior
            to using the library resources.
          </li>
          <li className="list-group-item">
            No personal belongings allowed inside the library Textbooks, printed
            materials and issued books are not allowed to be taken inside the
            library Using Mobile phones and audio instruments with or without
            speaker or headphone is strictly prohibited in the library premises.
          </li>
          <li className="list-group-item">
            Enter your name and Sign in the register kept at the entrance
            counter before entering library Show the books and other materials
            which are being taken out of the library to the staff at the
            entrance counter. The librarian may recall any book from any member
            at any time and the member shall return the same immediately.
          </li>
        </ul>
        <li className="list-group-item">
          Library borrower cards are not transferable. The borrower is
          responsible for the books borrowed on his/her card. Refreshment of any
          kind shall not be taken any where in the library premises Students are
          allowed to library only on production of their authorized/valid
          Identity Cards.
        </li>
        <li className="list-group-item">
          If the books are lost, Then the borrower shall replace the books of
          the same edition or latest edition or pay double cost of the book
          after getting permission from the librarian.
        </li>
        <li className="list-group-item">
          Take special care to maintain the library borrower cards. Do not fold,
          alter entries made on the cards, Members are responsible for the
          entire set of library borrower card issued to them.
        </li>
        <li className="list-group-item">
          Loss of borrower card should be reported to the librarian. Duplicate
          card may be issued against formal application and fine.
        </li>
        <li className="list-group-item">
          Each student shall obtain No dues certificate from the library after
          returning all the books issued, surrendering the borrower’s cards and
          after paying outstanding dues, if any.
        </li>
      </div>
    );
  }
}

export default Rules;
