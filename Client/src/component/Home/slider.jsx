import React, { Component } from "react";
import Swiper from "react-id-swiper";
import "swiper/css/swiper.css";
export class slider extends Component {
  render() {
    const params = {
      spaceBetween: 30,
      centeredSlides: true,
      loop: true,
      autoplay: {
        delay: 2500,
        disableOnInteraction: false
      },
      pagination: {
        el: ".swiper-pagination",
        clickable: true
      },
      navigation: {
        nextEl: ".swiper-button-next",
        prevEl: ".swiper-button-prev"
      }
    };
    return (
      <div>
        <Swiper {...params}>
          <div>
            <img
              className="slider_img"
              src={require("../../assets/images/1.jpg")}
              alt=""
            />
          </div>
          <div>
            <img
              className="slider_img"
              src={require("../../assets/images/2.jpg")}
              alt=""
            />
          </div>
          <div>
            <img
              className="slider_img"
              src={require("../../assets/images/3.jpg")}
              alt=""
            />
          </div>
          <div>
            <img
              className="slider_img"
              src={require("../../assets/images/4.jpg")}
              alt=""
            />
          </div>
        </Swiper>
      </div>
    );
  }
}

export default slider;
