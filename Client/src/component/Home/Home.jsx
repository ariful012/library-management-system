import React, { Component } from "react";
import Slider from "../Home/slider";
import Rules from "../Home/Rules";
import About from "../Home/About";
import Navbar from "../Home/Navbar";
import Footer from "../Home/Footer";

export class Home extends Component {
  render() {
    return (
      <React.Fragment>
        <Navbar />
        <Slider />
        <Rules />
        <About />
        <Footer />
      </React.Fragment>
    );
  }
}

export default Home;
