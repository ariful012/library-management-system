import React, { Component } from "react";

export class About extends Component {
  render() {
    return (
      <React.Fragment>
        <div className="row m-2">
          <div className="col-md-6">
            <div id="about" className="about">
              <div className="card">
                <div className="card-header text-center">About US</div>
                <div className="list-group list-group-flush">
                  <div className="list-group-item">
                    Being approved by the University Grants Commission of
                    Bangladesh (UGC) and the Government on 14 March 2012,
                    Hamdard University Bangladesh (HUB) started its academic
                    programs on 29 November 2012. It is a non-political and
                    not-for-profit organization, established under the Private
                    University Act 2010 (Law number 35).<br></br>
                    <br></br> Abiding by this Law, the core values of the
                    University are based on the philanthropic characteristics of
                    its affiliated/ mother organization—Hamdard Laboratories
                    (Waqf) Bangladesh and Hamdard Foundation—resonating love for
                    humanity, belief in pluralistic society, equal opportunity
                    for all, democratic decision making process, promotion of
                    education, technology and culture and the like.<br></br>{" "}
                    Hamdard University Bangladesh is the culmination of the
                    vision and brainchild of Dr. Hakim Mohammad Yousuf Harun
                    Bhuiyan. It was his long cherished desire to set up a “City
                    of Science, Education & Culture” and couch the University in
                    it as the central focus. Dr. Hakim Mohammad Yousuf Harun
                    Bhuiyan, the architect of Hamdard University Bangladesh, is
                    also the pioneer of modern Hamdard Bangladesh.<br></br> He
                    is the Managing Director of Hamdard Laboratories (Waqf)
                    Bangladesh and Secretary General of Hamdard Foundation
                    Bangladesh, the sole funding agency of this University.
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="col-md-6">
            <div id="contact" className="contact">
              <div className="card">
                <div className="card-header text-center">Contact US</div>
                <div className="list-group list-group-flush">
                  <div className="mapouter">
                    <div className="gmap_canvas">
                      <iframe
                        id="gmap_canvas"
                        src="https://maps.google.com/maps?q=hamdard%20university%20bangladesh&t=&z=13&ie=UTF8&iwloc=&output=embed"
                        frameBorder={0}
                        scrolling="no"
                        marginHeight={0}
                        marginWidth={0}
                      />
                      <a href="https://www.easy-ptable.com">periodic table</a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default About;
