import React from "react";
import { BrowserRouter as Router, Route } from "react-router-dom";
import "./assets/css/customfont.css";
import "./assets/sass/main.scss";

import Home from "./component/Home/Home";
import Login from "./component//Auth/Login";
import Registar from "./component//Auth/Registar";
function App() {
  return (
    <div className="App">
      <Router>
        <Route exact path="/" component={Home} />
        <Route exact path="/login" component={Login} />
        <Route exact path="/registar" component={Registar} />
      </Router>
    </div>
  );
}

export default App;
