const express = require('express')

const User = require("../../models/User")

const router = express.Router()


//@route  GET api/users/register
//@desc   Register A User
//@access Public
router.post('/register', (req, res) => {
    const { name, email, password, position } = req.body;
    const newUser = new User({
        name,
        email,
        password,
        position
    })
    newUser.save().then(user => {
        console.log(user)
    }).catch(err => {
        console.log(err)
    })



})

module.exports = router