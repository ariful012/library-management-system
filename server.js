const express = require("express");
const mongoose = require("mongoose");

const app = express();

//user routes
const users = require('./api/routes/users')

//body parser
const bodyParser = require("body-parser");
// use bodyParser
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());


//DB url
const db = require("./config/keys").mongoURI;
//connect to db
mongoose
  .connect(db, { useNewUrlParser: true })
  .then(() => console.log("MongoDB Connected!"))
  .catch(err => {
    console.log(err);
  });


app.use('/api/users/', users)



const port = process.env.PORT || 9000;

app.listen(port, () => {
  console.log(`Server is starting at port ${port}`);
});
